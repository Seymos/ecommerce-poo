<?php

namespace Models;

class Comment extends Model
{
    protected $table = "comments";

    /**
     * @param int $article_id
     * @return array $commentaires
     */
    public function findAllWithArticle(int $article_id)
    {
        $query = $this->pdo->prepare("SELECT * FROM comments WHERE article_id = :article_id");
        $query->execute(['article_id' => $article_id]);
        $commentaires = $query->fetchAll();
        return $commentaires;
    }

    /**
     * @param string $author
     * @param string $content
     * @param string $article_id
     */
    public function save(string $author, string $content, int $article_id)
    {
        $query = $this->pdo->prepare('INSERT INTO comments SET author = :author, content = :content, article_id = :article_id, created_at = NOW()');
        $query->execute(compact('author', 'content', 'article_id'));
    }
}
