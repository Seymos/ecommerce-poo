<?php

namespace Controllers;

class Article extends Controller
{
    protected $modelName = \Models\Article::class;

    public function index()
    {
        $articles = $this->model->findAll("created_at DESC");
        $pageTitle = "Accueil";
        \Renderer::render(
            'articles/index',
            compact('pageTitle', 'articles')
        );
    }

    public function show()
    {
        $article_id = null;
        if (!empty($_GET['id']) && ctype_digit($_GET['id'])) {
            $article_id = $_GET['id'];
        }
        if (!$article_id) {
            die("Vous devez préciser un paramètre `id` dans l'URL !");
        }
        $commentModel = new \Models\Comment();
        $article = $this->model->find($article_id);

        /**
         * 4. Récupération des commentaires de l'article en question
         * Pareil, toujours une requête préparée pour sécuriser la donnée filée par l'utilisateur (cet enfoiré en puissance !)
         */
        $commentaires = $commentModel->findAllWithArticle($article_id);

        /**
         * 5. On affiche
         */
        $pageTitle = $article['title'];
        \Renderer::render(
            'articles/show',
            compact('pageTitle', 'article_id', 'article', 'commentaires')
        );
    }

    public function delete()
    {
        /**
         * 1. On vérifie que le GET possède bien un paramètre "id" (delete.php?id=202) et que c'est bien un nombre
         */
        if (empty($_GET['id']) || !ctype_digit($_GET['id'])) {
            die("Ho ?! Tu n'as pas précisé l'id de l'article !");
        }

        $id = $_GET['id'];

        /**
         * 3. Vérification que l'article existe bel et bien
         */
        $article = $this->model->find($id);
        if (!$article) {
            die("L'article $id n'existe pas, vous ne pouvez donc pas le supprimer !");
        }

        /**
         * 4. Réelle suppression de l'article
         */
        $this->model->delete($id);

        \Http::redirect('index.php');

    }
}